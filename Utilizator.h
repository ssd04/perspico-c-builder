//---------------------------------------------------------------------------


#include <vcl.h>

#ifndef UtilizatorH
#define UtilizatorH
//---------------------------------------------------------------------------
#endif


class Utilizator{
public:
        AnsiString nume;
        AnsiString parola;

        Utilizator();
        Utilizator(AnsiString, AnsiString);

        ~Utilizator();

        void SchimbaParola(AnsiString);
        bool ParolaValida(AnsiString);
        bool UtilizatorValid(AnsiString, AnsiString) ;
};