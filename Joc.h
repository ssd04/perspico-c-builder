//---------------------------------------------------------------------------

#include <list.h>
using namespace std;
#include "Jucator.h"
#ifndef JocH
#define JocH
//---------------------------------------------------------------------------
#endif

enum TipJucatorPerspico  { JucatorPasiv, JucatorCareAlege };

class JucatorPerspico: public Jucator{
        TipJucatorPerspico tip;
};

class JocGeneric {
public:
        list <Jucator> Jucatori;

        JocGeneric();
        virtual void InitializeazaJoc()=0;
        virtual void Start()=0;
        virtual bool Castigator(Jucator) = 0;

};

class Perspico:public JocGeneric{
 public:
        int numarPioni;
        int numarIncercari;
        int pioniDeGhicit[10];
        int pioniCurenti[10];
        int raspuns[10];

        
        void SelecteazaPioniDeGhicit(Jucator J){
                TFormaDeSelectat * formaDeSelectat = new TFormaDeSelectat(this);
                formaDeSelectat->ShowModal();


        }

        void InitializeazaJoc(){
                numarPioni = 5;
                numarIncercari = 7;
             SelecteazaJucatori();
             SelecteazaPioniDeGhicit(Jucatori[0]);
        }

        bool ConditieDeOprire(){
                return numarIncercari==0;
        }


        void Start(){
                InitializeazaJoc();
                while (!ConditieDeOprire()){
                        SelecteazaPioniCurenti(Jucatori[1]);
                        if (ConditieDeCastig(Jucatori[1])){
                                // ce faci daca a castigat
                        }
                        DaRaspuns(Jucator[0]);
                        numarIncercari--;
                }
        }
        
}