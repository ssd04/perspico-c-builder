//---------------------------------------------------------------------------
#pragma once

#pragma hdrstop

#include "Jucator.h"
#include "LoginFrm.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

Jucator::Jucator():Utilizator(){
        highScore = 0;
}

 Jucator:: Jucator(AnsiString n, AnsiString p, int h):Utilizator(n,p){
        highScore = h;
 }
