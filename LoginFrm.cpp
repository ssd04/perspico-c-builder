//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "LoginFrm.h"
#include "Jucator.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TLoginForm *LoginForm;
//---------------------------------------------------------------------------
__fastcall TLoginForm::TLoginForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TLoginForm::FormMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
        this->Caption =  IntToStr(X) + " " + IntToStr(Y);
        this->Color = TColor(RGB(X,Y,(X+Y)/2))     ;
}
//---------------------------------------------------------------------------
void __fastcall TLoginForm::loginButtonClick(TObject *Sender)
{
        Jucator U;
        if (U.UtilizatorValid(numeEditBox->Text, parolaEditBox->Text)){
                ShowMessage("Intra in joc");
        }
        else{
              ShowMessage("Nume sau parola invalide");
        }
}
//---------------------------------------------------------------------------
