//---------------------------------------------------------------------------


#pragma hdrstop

#include "Utilizator.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

        Utilizator::Utilizator(){
                nume = "Default";
                parola = "";
        }

        Utilizator::Utilizator(AnsiString n, AnsiString p){
                nume = n;
                parola = p;
        }

        Utilizator::~Utilizator(){
                // sunt distrus
        }

        void Utilizator::SchimbaParola(AnsiString parolaNoua){
               parola = parolaNoua;
        }

        bool Utilizator::ParolaValida(AnsiString p){
              return parola == p;
        }

        
        bool Utilizator::UtilizatorValid(AnsiString n, AnsiString p) {
               return nume==n && parola==p;
        }
