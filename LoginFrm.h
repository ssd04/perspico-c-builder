//---------------------------------------------------------------------------

#ifndef LoginFrmH
#define LoginFrmH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <jpeg.hpp>
//---------------------------------------------------------------------------
class TLoginForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TEdit *numeEditBox;
        TEdit *parolaEditBox;
        TLabel *Label2;
        TImage *Image1;
        TButton *loginButton;
        void __fastcall FormMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall loginButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TLoginForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TLoginForm *LoginForm;
//---------------------------------------------------------------------------
#endif
