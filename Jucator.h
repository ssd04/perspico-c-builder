//---------------------------------------------------------------------------
#pragma once

#include "Utilizator.h"

#ifndef JucatorH
#define JucatorH
//---------------------------------------------------------------------------
#endif


class Jucator:public Utilizator{
public:
        int highScore;

        Jucator();
        Jucator(AnsiString, AnsiString, int);
};